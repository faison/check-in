<?php
/**
 * The visit endpoint template.
 *
 * This template is based off of the twentyseventeen page template and content-page partial.
 *
 * @since 1.0.0
 */

get_header(); ?>

	<div class="wrap">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?php echo esc_html__( 'Visitor Check-in', 'check_in' ); ?></h1>
					</header><!-- .entry-header -->
					<div class="entry-content">
						<div id="root"></div>
					</div><!-- .entry-content -->
				</article><!-- #post-## -->

			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- .wrap -->

<?php get_footer();
