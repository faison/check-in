/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _CheckInForm = __webpack_require__(1);

	var _CheckInForm2 = _interopRequireDefault(_CheckInForm);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var React = window.React; /**
	                           * The main Check-in form JS file, responsible for starting up the Check-in Form.
	                           *
	                           * @summary The main Check-in form JS file, responsible for starting up the Check-in Form.
	                           *
	                           * @since 1.0.0
	                           */

	var ReactDOM = window.ReactDOM;

	ReactDOM.render(React.createElement(_CheckInForm2.default, null), document.getElementById('root'));

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _data = __webpack_require__(2);

	var _data2 = _interopRequireDefault(_data);

	var _validation = __webpack_require__(3);

	var _validation2 = _interopRequireDefault(_validation);

	var _submission = __webpack_require__(4);

	var _submission2 = _interopRequireDefault(_submission);

	var _ConfirmationMessage = __webpack_require__(5);

	var _ConfirmationMessage2 = _interopRequireDefault(_ConfirmationMessage);

	var _SubmitButton = __webpack_require__(6);

	var _SubmitButton2 = _interopRequireDefault(_SubmitButton);

	var _ActionLink = __webpack_require__(7);

	var _ActionLink2 = _interopRequireDefault(_ActionLink);

	var _FormField = __webpack_require__(8);

	var _FormField2 = _interopRequireDefault(_FormField);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * The CheckInForm React Component and its property definitions.
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @summary The CheckInForm React Component and its property definitions.
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @since 1.0.0
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @module components/CheckInForm
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

	var React = window.React;

	/**
	 * The CheckInForm React Component.
	 *
	 * @summary The CheckInForm React Component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @extends React.Component
	 */

	var CheckInForm = function (_React$Component) {
		_inherits(CheckInForm, _React$Component);

		/**
	  * Used to initiate the Check-in Form component, binding its context with its callback functions
	  * and setting its initial state.
	  *
	  * @summary Used to initiate the Check-in Form component.
	  *
	  * @since 1.0.0
	  * @access public
	  *
	  * @param {Object} props - The Component Props the CheckInForm was initiated with.
	  */
		function CheckInForm(props) {
			_classCallCheck(this, CheckInForm);

			var _this = _possibleConstructorReturn(this, (CheckInForm.__proto__ || Object.getPrototypeOf(CheckInForm)).call(this, props));

			_this.state = {
				yourName: '',
				yourEmail: '',
				visiting: '',
				loading: false,
				showErrors: false,
				generalError: '',
				showConfirmation: _this.props.showConfirmation
			};

			_this.fieldChangeHandler = _this.fieldChangeHandler.bind(_this);
			_this.submissionHandler = _this.submissionHandler.bind(_this);
			_this.confirmationTimerFinished = _this.confirmationTimerFinished.bind(_this);
			_this.submissionCallback = _this.submissionCallback.bind(_this);
			return _this;
		}

		/**
	  * Callback function that updates the Check-in Form's value state for a field..
	  *
	  * @summary Callback function that updates the Check-in Form's value state for a field.
	  *
	  * @since 1.0.0
	  * @access public
	  *
	  * @param {string}        name  - The field name.
	  * @param {string|number} value - The field value.
	  */


		_createClass(CheckInForm, [{
			key: 'fieldChangeHandler',
			value: function fieldChangeHandler(name, value) {
				var newState = {};

				newState[name] = value;

				this.setState(newState);
			}

			/**
	   * Handles a form submission, showing errors if there are any or showing the Confirmation Message.
	   * The Confirmation Message will only show if the form data is successfully submitted to the site.
	   * Otherwise, show a server error.
	   *
	   * @summary Handles a form submission, showing errors if there are any or showing the Confirmation Message.
	   *
	   * @since 1.0.0
	   * @access public
	   *
	   * @param {Object} e - The event object.
	   */

		}, {
			key: 'submissionHandler',
			value: function submissionHandler(e) {
				e.preventDefault();

				var nameValid = _validation2.default.validateText(this.state.yourName);
				var emailValid = _validation2.default.validateEmail(this.state.yourEmail);
				var visitingValid = _validation2.default.validateCoworkerDesk(this.state.visiting);

				if (!(nameValid && emailValid && visitingValid)) {
					this.setState({
						showErrors: true
					});

					return;
				}

				this.setState({
					loading: true
				});

				_submission2.default.submitCheckIn({
					yourName: this.state.yourName,
					yourEmail: this.state.yourEmail,
					visiting: this.state.visiting
				}, this.submissionCallback);
			}

			/**
	   * The callback function for the ConfirmationMessage's timer trigger. Resets the forms.
	   *
	   * @summary The callback function for the ConfirmationMessage's timer trigger.
	   *
	   * @since 1.0.0
	   * @access public
	   */

		}, {
			key: 'confirmationTimerFinished',
			value: function confirmationTimerFinished() {
				this.setState({
					yourName: '',
					yourEmail: '',
					visiting: '',
					loading: false,
					showErrors: false,
					generalError: '',
					showConfirmation: false
				});
			}

			/**
	   * The callback function for a submission response.
	   *
	   * @summary The callback function for a submission response.
	   *
	   * @since 1.0.0
	   * @access public
	   *
	   * @param {bool}   success - Whether or not the submission was a success.
	   * @param {Object} context - Context for the success or failure
	   */

		}, {
			key: 'submissionCallback',
			value: function submissionCallback(success, context) {
				if (success) {
					this.setState({
						showConfirmation: true
					});

					return;
				}

				var newState = {
					loading: false,
					showErrors: true
				};

				if ('invalid_nonce' === context.code) {
					newState.generalError = 'Invalid Nonce, please refresh page';
				} else if ('db_add_log' === context.code) {
					newState.generalError = 'Database connection error, please try again shortly';
				} else if ('unexpected_add_log' === context.code) {
					newState.generalError = 'An unexpected error occurred , please try again shortly';
				} else if ('visitor_already_checked_in' === context.code) {
					newState.generalError = 'We\'re sorry, but you can only check in once per day';
				} else if ('invalid_meta_values' === context.code) {
					newState.generalError = 'Please check your field values and refresh if there are still issues';
				}

				this.setState(newState);
			}

			/**
	   * Renders the Check-in Form. This switches between a Confirmation message and the actual form.
	   *
	   * @summary Renders the Check-in Form.
	   *
	   * @since 1.0.0
	   * @access public
	   *
	   * @returns {XML} The Check-in Form.
	   */

		}, {
			key: 'render',
			value: function render() {
				if (this.state.showConfirmation) {
					return React.createElement(
						'div',
						null,
						React.createElement(_ConfirmationMessage2.default, { secondsLeft: 3, timerFinished: this.confirmationTimerFinished })
					);
				}

				var formClass = 'check-in-form';
				var generalNote = React.createElement('div', { className: 'general-note' });
				var nameValid = true;
				var emailValid = true;
				var visitingValid = true;
				var submitText = 'Check In';
				var actionLink = '';

				var loading = this.state.loading;

				if (loading) {
					formClass += ' loading';
					submitText = 'Checking in...';
				} else {
					actionLink = React.createElement(_ActionLink2.default, { label: 'Reset', disabled: loading, onClick: this.confirmationTimerFinished });
				}

				if (this.state.showErrors) {
					nameValid = _validation2.default.validateText(this.state.yourName);
					emailValid = _validation2.default.validateEmail(this.state.yourEmail);
					visitingValid = _validation2.default.validateCoworkerDesk(this.state.visiting);

					if (this.state.generalError) {
						generalNote = React.createElement(
							'div',
							{ className: 'general-note checkin-error' },
							this.state.generalError
						);
					}
				}

				return React.createElement(
					'form',
					{ className: formClass, onSubmit: this.submissionHandler },
					generalNote,
					React.createElement(_FormField2.default, { name: 'yourName', label: 'Your Name', type: 'text', value: this.state.yourName,
						required: true, errorMessage: 'Please enter your name', onChange: this.fieldChangeHandler,
						valid: nameValid, disabled: loading }),
					React.createElement(_FormField2.default, { name: 'yourEmail', label: 'Your Email', type: 'text', value: this.state.yourEmail,
						required: true, errorMessage: 'Please enter a valid email', onChange: this.fieldChangeHandler,
						valid: emailValid, disabled: loading }),
					React.createElement(_FormField2.default, { name: 'visiting', label: 'Who are you visiting?', type: 'select',
						required: true, errorMessage: 'Please select an option',
						value: this.state.visiting, options: _data2.default.getCoworkers(), onChange: this.fieldChangeHandler,
						valid: visitingValid, disabled: loading }),
					React.createElement(_SubmitButton2.default, { label: submitText, disabled: loading }),
					actionLink
				);
			}
		}]);

		return CheckInForm;
	}(React.Component);

	exports.default = CheckInForm;


	CheckInForm.propTypes = {
		showConfirmation: React.PropTypes.bool
	};

	CheckInForm.defaultProps = {
		showConfirmation: false
	};

/***/ },
/* 2 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * Functions used for accessing data localized from WordPress.
	 *
	 * @summary Functions used for accessing data localized from WordPress.
	 *
	 * @since 1.0.0
	 */

	var data = window.check_in_data || {};

	/**
	 * Returns an array of Coworkers.
	 *
	 * @summary Returns an array of Coworkers.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {Object[]} An array of Coworkers.
	 */
	function getCoworkers() {
	  if (data.coworkers) {
	    return data.coworkers;
	  }

	  return [];
	}

	/**
	 * Returns the nonce used for submissions.
	 *
	 * @summary Returns the nonce used for submissions.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {string} The nonce used for submissions.
	 */
	function getNonce() {
	  if (data.nonce) {
	    return data.nonce;
	  }

	  return '';
	}

	/**
	 * Returns the nonce name used for submissions.
	 *
	 * @summary Returns the nonce name used for submissions.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {string} The nonce name used for submissions.
	 */
	function getNonceName() {
	  if (data.nonce_name) {
	    return data.nonce_name;
	  }

	  return '';
	}

	/**
	 * Returns the submission flag used for submissions.
	 *
	 * @summary Returns the submission flag used for submissions.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {string} The submission flag used for submissions.
	 */
	function getSubmissionFlag() {
	  if (data.submission_flag) {
	    return data.submission_flag;
	  }

	  return '';
	}

	/**
	 * Returns the submission URL used for submissions.
	 *
	 * @summary Returns the submission URL used for submissions.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {string} The submission URL used for submissions.
	 */
	function getSubmissionUrl() {
	  if (data.submission_url) {
	    return data.submission_url;
	  }

	  return '';
	}

	exports.default = {
	  getCoworkers: getCoworkers, getNonce: getNonce, getNonceName: getNonceName, getSubmissionFlag: getSubmissionFlag, getSubmissionUrl: getSubmissionUrl
	};

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _data = __webpack_require__(2);

	var _data2 = _interopRequireDefault(_data);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	/**
	 * Checks if text is valid. Only one character is required to be valid.
	 *
	 * @summary Checks if text is valid.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {string} text - The text to validate.
	 *
	 * @returns {boolean} true if valid, false if not.
	 */
	function validateText(text) {
	  var value = text.trim();

	  if (!value) {
	    return false;
	  }

	  return true;
	}

	/**
	 * Checks if an email has a valid structure.
	 * This is general checking to make sure the email follows an <account>@<domain> format.
	 *
	 * @summary Checks if an email has a valid structure.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {string} email - The email to validate.
	 *
	 * @returns {boolean} true if valid, false if not.
	 */
	/**
	 * Validation function for use in the Check-in form.
	 *
	 * @summary Validation function for use in the Check-in form.
	 *
	 * @since 1.0.0
	 */

	function validateEmail(email) {
	  var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
	  return re.test(email);
	}

	/**
	 * Checks if a coworker desk is valid.
	 *
	 * @summary Checks if a coworker desk is valid.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {number|string} desk - The coworker desk number to validate.
	 *
	 * @returns {boolean} true if valid, false if not.
	 */
	function validateCoworkerDesk(desk) {
	  var value = parseInt(desk, 10);

	  if (isNaN(value)) {
	    return false;
	  }

	  var found = _data2.default.getCoworkers().find(function (element) {
	    return element.desk === value;
	  });

	  if (!found) {
	    return false;
	  }

	  return true;
	}

	exports.default = {
	  validateText: validateText, validateEmail: validateEmail, validateCoworkerDesk: validateCoworkerDesk
	};

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _data = __webpack_require__(2);

	var _data2 = _interopRequireDefault(_data);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var $ = window.jQuery;

	/**
	 * Submits visitor information to the server to be logged.
	 *
	 * @summary Submits visitor information to the server to be logged.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {Object}   args     - The visitor information to log.
	 * @param {Function} callback - The callback function for submission responses.
	 */
	/**
	 * Functions used to make submissions.
	 *
	 * @summary Functions used to make submissions.
	 *
	 * @since 1.0.0
	 */

	function submitCheckIn(args, callback) {
		var postData = {
			'visitor_name': args.yourName,
			'visitor_email': args.yourEmail,
			'visitor_desk': args.visiting
		};

		postData[_data2.default.getNonceName()] = _data2.default.getNonce();
		postData[_data2.default.getSubmissionFlag()] = 1;

		$.post(_data2.default.getSubmissionUrl(), postData, function (response) {
			if (response.success) {
				callback(true);
			}

			var errorInfo = {
				'code': response.data.error,
				'data': response.data.data
			};

			if ('invalid_meta_values' === errorInfo.code) {
				var newData = [];

				//This is painful to look at and should be refactored to be less hardcoded at some time in the future
				if (errorInfo.data.includes('visitor_name')) {
					newData.push('yourName');
				}

				if (errorInfo.data.includes('visitor_email')) {
					newData.push('yourEmail');
				}

				if (errorInfo.data.includes('visitor_desk')) {
					newData.push('visiting');
				}

				errorInfo.data = newData;
			}

			callback(false, errorInfo);
		});
	}

	exports.default = {
		submitCheckIn: submitCheckIn
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	/**
	 * The ConfirmationMessage React Component and its property definitions.
	 *
	 * @summary The ConfirmationMessage React Component and its property definitions.
	 *
	 * @since 1.0.0
	 *
	 * @module components/ConfirmationMessage
	 */

	var React = window.React;

	/**
	 * The ConfirmationMessage React Component.
	 *
	 * @summary The ConfirmationMessage React Component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @extends React.Component
	 */

	var ConfirmationMessage = function (_React$Component) {
		_inherits(ConfirmationMessage, _React$Component);

		/**
	  * Used to initiate the Confirmation Message component, binding its context with its callback functions
	  * and setting its initial state.
	  *
	  * @summary Used to initiate the Confirmation Message component.
	  *
	  * @since 1.0.0
	  * @access public
	  *
	  * @param {Object} props - The Component Props the ConfirmationMessage was initiated with.
	  */
		function ConfirmationMessage(props) {
			_classCallCheck(this, ConfirmationMessage);

			var _this = _possibleConstructorReturn(this, (ConfirmationMessage.__proto__ || Object.getPrototypeOf(ConfirmationMessage)).call(this, props));

			_this.state = {
				secondsLeft: _this.props.secondsLeft
			};

			_this.tick = _this.tick.bind(_this);
			return _this;
		}

		/**
	  * Starts the interval used for the Confirmation Message's secondsLeft counter.
	  *
	  * @summary Starts the interval used for the Confirmation Message's secondsLeft counter.
	  *
	  * @since 1.0.0
	  * @access public
	  */


		_createClass(ConfirmationMessage, [{
			key: "componentDidMount",
			value: function componentDidMount() {
				var _this2 = this;

				this.intervalId = setInterval(function () {
					return _this2.tick();
				}, 1000);
			}

			/**
	   * Clears out the interval used for the Confirmation Message's secondsLeft counter.
	   *
	   * @summary Clears out the interval used for the Confirmation Message's secondsLeft counter.
	   *
	   * @since 1.0.0
	   * @access public
	   */

		}, {
			key: "componentWillUnmount",
			value: function componentWillUnmount() {
				clearInterval(this.intervalId);
			}

			/**
	   * Decrements the Confirmation Message's secondsLeft counter, triggering if zero is reached.
	   *
	   * @summary Decrements the Confirmation Message's secondsLeft counter, triggering if zero is reached.
	   *
	   * @since 1.0.0
	   * @access public
	   */

		}, {
			key: "tick",
			value: function tick() {
				this.setState(function (prevState) {
					return {
						secondsLeft: prevState.secondsLeft - 1
					};
				});

				if (0 >= this.state.secondsLeft) {
					this.props.timerFinished();
				}
			}

			/**
	   * Renders the Confirmation Message.
	   *
	   * @summary Renders the Confirmation Message.
	   *
	   * @since 1.0.0
	   * @access public
	   *
	   * @returns {XML} The Confirmation Message JSX.
	   */

		}, {
			key: "render",
			value: function render() {
				return React.createElement(
					"div",
					null,
					React.createElement(
						"p",
						null,
						React.createElement(
							"strong",
							null,
							"Thank you for checking in!"
						)
					),
					React.createElement(
						"p",
						null,
						React.createElement(
							"em",
							null,
							"This form will reset in ",
							this.state.secondsLeft,
							" seconds"
						)
					)
				);
			}
		}]);

		return ConfirmationMessage;
	}(React.Component);

	exports.default = ConfirmationMessage;


	ConfirmationMessage.propTypes = {
		secondsLeft: React.PropTypes.number,
		timerFinished: React.PropTypes.func
	};

/***/ },
/* 6 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	/**
	 * The SubmitButton React Component and its property definitions.
	 *
	 * @summary The SubmitButton React Component and its property definitions.
	 *
	 * @since 1.0.0
	 *
	 * @module components/SubmitButton
	 */

	var React = window.React;

	/**
	 * The SubmitButton React Component.
	 *
	 * @summary The SubmitButton React Component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @extends React.Component
	 */

	var SubmitButton = function (_React$Component) {
	  _inherits(SubmitButton, _React$Component);

	  function SubmitButton() {
	    _classCallCheck(this, SubmitButton);

	    return _possibleConstructorReturn(this, (SubmitButton.__proto__ || Object.getPrototypeOf(SubmitButton)).apply(this, arguments));
	  }

	  _createClass(SubmitButton, [{
	    key: "render",

	    /**
	     * Renders the Submit Button.
	     *
	     * @summary Renders the Submit Button.
	     *
	     * @since 1.0.0
	     * @access public
	     *
	     * @returns {XML} The Submit button JSX.
	     */
	    value: function render() {
	      return React.createElement("input", { type: "submit", value: this.props.label, disabled: this.props.disabled });
	    }
	  }]);

	  return SubmitButton;
	}(React.Component);

	exports.default = SubmitButton;


	SubmitButton.propTypes = {
	  label: React.PropTypes.string,
	  disabled: React.PropTypes.bool
	};

/***/ },
/* 7 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	/**
	 * The ActionLink React Component and its property definitions.
	 *
	 * @summary The ActionLink React Component and its property definitions.
	 *
	 * @since 1.0.0
	 *
	 * @module components/ActionLink
	 */

	var React = window.React;

	/**
	 * The ActionLink React Component.
	 *
	 * @summary The ActionLink React Component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @extends React.Component
	 */

	var ActionLink = function (_React$Component) {
		_inherits(ActionLink, _React$Component);

		/**
	  * Used to initiate the Action Link component, binding its context with its callback functions.
	  *
	  * @summary Used to initiate the Action Link component.
	  *
	  * @since 1.0.0
	  * @access public
	  *
	  * @param {Object} props - The Component Props the ActionLink was initiated with.
	  */
		function ActionLink(props) {
			_classCallCheck(this, ActionLink);

			var _this = _possibleConstructorReturn(this, (ActionLink.__proto__ || Object.getPrototypeOf(ActionLink)).call(this, props));

			_this.clickHandler = _this.clickHandler.bind(_this);
			return _this;
		}

		_createClass(ActionLink, [{
			key: "clickHandler",
			value: function clickHandler(e) {
				e.preventDefault();

				this.props.onClick();
			}

			/**
	   * Renders the Action Link.
	   *
	   * @summary Renders the Action Link.
	   *
	   * @since 1.0.0
	   * @access public
	   *
	   * @returns {XML} The Action Link JSX.
	   */

		}, {
			key: "render",
			value: function render() {
				return React.createElement(
					"div",
					{ className: "action-link" },
					React.createElement(
						"a",
						{ href: "#", disabled: this.props.disabled, onClick: this.clickHandler },
						this.props.label
					)
				);
			}
		}]);

		return ActionLink;
	}(React.Component);

	exports.default = ActionLink;


	ActionLink.propTypes = {
		label: React.PropTypes.string,
		disabled: React.PropTypes.bool,
		onClick: React.PropTypes.func
	};

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _TextField = __webpack_require__(9);

	var _TextField2 = _interopRequireDefault(_TextField);

	var _SelectField = __webpack_require__(10);

	var _SelectField2 = _interopRequireDefault(_SelectField);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * The FormField React Component and its property definitions.
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @summary The FormField React Component and its property definitions.
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @since 1.0.0
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @module components/FormField
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @see components/TextField
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * @see components/SelectField
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

	var React = window.React;

	/**
	 * The FormField React Component.
	 *
	 * @summary The FormField React Component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @extends React.Component
	 */

	var FormField = function (_React$Component) {
		_inherits(FormField, _React$Component);

		function FormField() {
			_classCallCheck(this, FormField);

			return _possibleConstructorReturn(this, (FormField.__proto__ || Object.getPrototypeOf(FormField)).apply(this, arguments));
		}

		_createClass(FormField, [{
			key: 'render',

			/**
	   * Renders the Form Field. This renders a different field depending on the type provided.
	   *
	   * @summary Renders the Form Field.
	   *
	   * @since 1.0.0
	   * @access public
	   *
	   * @returns {XML} The Form Field JSX.
	   */
			value: function render() {
				var name = this.props.name;
				var type = this.props.type;
				var value = this.props.value;
				var disabled = this.props.disabled;

				var required = '';
				var errorMessage = '';
				var field = '';

				if (this.props.required) {
					required = '*';
				}

				if (!this.props.valid) {
					errorMessage = React.createElement(
						'span',
						{ className: 'checkin-error' },
						this.props.errorMessage
					);
				}

				if ('text' === type) {
					field = React.createElement(_TextField2.default, { name: name, value: value, onChange: this.props.onChange, disabled: disabled });
				} else if ('select' === type) {
					field = React.createElement(_SelectField2.default, { name: name, value: value, options: this.props.options,
						onChange: this.props.onChange, disabled: disabled });
				}

				return React.createElement(
					'div',
					{ className: 'form-field' },
					React.createElement(
						'label',
						{ htmlFor: name },
						this.props.label,
						' ',
						required,
						' ',
						errorMessage
					),
					field
				);
			}
		}]);

		return FormField;
	}(React.Component);

	exports.default = FormField;


	FormField.propTypes = {
		name: React.PropTypes.string,
		type: React.PropTypes.string,
		value: React.PropTypes.string,
		required: React.PropTypes.bool,
		errorMessage: React.PropTypes.string,
		valid: React.PropTypes.bool,
		options: React.PropTypes.array,
		label: React.PropTypes.string,
		onChange: React.PropTypes.func,
		disabled: React.PropTypes.bool
	};

/***/ },
/* 9 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	/**
	 * The TextField React Component and its property definitions.
	 *
	 * @summary The TextField React Component and its property definitions.
	 *
	 * @since 1.0.0
	 *
	 * @module components/TextField
	 * @see components/FormField
	 */

	var React = window.React;

	/**
	 * The TextField React Component.
	 *
	 * @summary The TextField React Component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @extends React.Component
	 */

	var TextField = function (_React$Component) {
		_inherits(TextField, _React$Component);

		/**
	  * Used to initiate the Text Field component, binding its context with its callback functions.
	  *
	  * @summary Used to initiate the Text Field component.
	  *
	  * @since 1.0.0
	  * @access public
	  *
	  * @param {Object} props - The Component Props the TextField was initiated with.
	  */
		function TextField(props) {
			_classCallCheck(this, TextField);

			var _this = _possibleConstructorReturn(this, (TextField.__proto__ || Object.getPrototypeOf(TextField)).call(this, props));

			_this.onChangeHandler = _this.onChangeHandler.bind(_this);
			return _this;
		}

		/**
	  * Used to bubble the Text Field's new value up the Component hierarchy.
	  *
	  * @summary Used to bubble the Text Field's new value up the Component hierarchy.
	  *
	  * @since 1.0.0
	  * @access public
	  *
	  * @param {Object} e - The event object.
	  */


		_createClass(TextField, [{
			key: "onChangeHandler",
			value: function onChangeHandler(e) {
				var name = this.props.name;
				var value = e.target.value;

				this.props.onChange(name, value);
			}

			/**
	   * Renders the Text Field.
	   *
	   * @summary Renders the Text Field.
	   *
	   * @since 1.0.0
	   * @access public
	   *
	   * @returns {XML} The Text Field JSX.
	   */

		}, {
			key: "render",
			value: function render() {
				return React.createElement("input", { type: "text", id: this.props.name, value: this.props.value,
					onChange: this.onChangeHandler, disabled: this.props.disabled });
			}
		}]);

		return TextField;
	}(React.Component);

	exports.default = TextField;


	TextField.propTypes = {
		name: React.PropTypes.string,
		value: React.PropTypes.string,
		onChange: React.PropTypes.func,
		disabled: React.PropTypes.bool
	};

/***/ },
/* 10 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	/**
	 * The SelectField React Component and its property definitions.
	 *
	 * @summary The SelectField React Component and its property definitions.
	 *
	 * @since 1.0.0
	 *
	 * @module components/SelectField
	 * @see components/FormField
	 */

	var React = window.React;

	/**
	 * The SelectField React Component.
	 *
	 * @summary The SelectField React Component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @extends React.Component
	 */

	var SelectField = function (_React$Component) {
		_inherits(SelectField, _React$Component);

		/**
	  * Used to initiate the Select Field component, binding its context with its callback functions.
	  *
	  * @summary Used to initiate the Select Field component.
	  *
	  * @since 1.0.0
	  * @access public
	  *
	  * @param {Object} props - The Component Props the SelectField was initiated with.
	  */
		function SelectField(props) {
			_classCallCheck(this, SelectField);

			var _this = _possibleConstructorReturn(this, (SelectField.__proto__ || Object.getPrototypeOf(SelectField)).call(this, props));

			_this.onChangeHandler = _this.onChangeHandler.bind(_this);
			return _this;
		}

		/**
	  * Used to bubble the Select Field's new value up the Component hierarchy.
	  *
	  * @summary Used to bubble the Select Field's new value up the Component hierarchy.
	  *
	  * @since 1.0.0
	  * @access public
	  *
	  * @param {Object} e - The event object.
	  */


		_createClass(SelectField, [{
			key: "onChangeHandler",
			value: function onChangeHandler(e) {
				var name = this.props.name;
				var value = e.target.value;

				this.props.onChange(name, value);
			}

			/**
	   * Renders the Select Field.
	   *
	   * @summary Renders the Select Field.
	   *
	   * @since 1.0.0
	   * @access public
	   *
	   * @returns {XML} The Select Field JSX.
	   */

		}, {
			key: "render",
			value: function render() {
				var options = this.props.options.map(function (option) {
					return React.createElement(
						"option",
						{ value: option.desk, key: option.desk },
						option.name
					);
				});

				return React.createElement(
					"select",
					{ id: this.props.name, value: this.props.value, onChange: this.onChangeHandler, disabled: this.props.disabled },
					React.createElement("option", { value: "" }),
					options
				);
			}
		}]);

		return SelectField;
	}(React.Component);

	exports.default = SelectField;


	SelectField.propTypes = {
		options: React.PropTypes.array,
		name: React.PropTypes.string,
		value: React.PropTypes.string,
		onChange: React.PropTypes.func,
		disabled: React.PropTypes.bool
	};

/***/ }
/******/ ]);