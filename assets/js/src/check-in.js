/**
 * The main Check-in form JS file, responsible for starting up the Check-in Form.
 *
 * @summary The main Check-in form JS file, responsible for starting up the Check-in Form.
 *
 * @since 1.0.0
 */

import CheckInForm from './check-in/components/CheckInForm';

let React    = window.React;
let ReactDOM = window.ReactDOM;

ReactDOM.render(
	<CheckInForm />,
	document.getElementById( 'root' )
);
