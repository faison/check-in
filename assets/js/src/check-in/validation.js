/**
 * Validation function for use in the Check-in form.
 *
 * @summary Validation function for use in the Check-in form.
 *
 * @since 1.0.0
 */

import data from './data.js';

/**
 * Checks if text is valid. Only one character is required to be valid.
 *
 * @summary Checks if text is valid.
 *
 * @since 1.0.0
 * @access public
 *
 * @param {string} text - The text to validate.
 *
 * @returns {boolean} true if valid, false if not.
 */
function validateText( text ) {
	let value = text.trim();

	if ( ! value ) {
		return false;
	}

	return true;
}

/**
 * Checks if an email has a valid structure.
 * This is general checking to make sure the email follows an <account>@<domain> format.
 *
 * @summary Checks if an email has a valid structure.
 *
 * @since 1.0.0
 * @access public
 *
 * @param {string} email - The email to validate.
 *
 * @returns {boolean} true if valid, false if not.
 */
function validateEmail( email ) {
	let re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
	return re.test( email );
}

/**
 * Checks if a coworker desk is valid.
 *
 * @summary Checks if a coworker desk is valid.
 *
 * @since 1.0.0
 * @access public
 *
 * @param {number|string} desk - The coworker desk number to validate.
 *
 * @returns {boolean} true if valid, false if not.
 */
function validateCoworkerDesk( desk ) {
	let value = parseInt( desk, 10 );

	if ( isNaN( value ) ) {
		return false;
	}

	let found = data.getCoworkers().find(( element ) => {
		return element.desk === value;
	});

	if ( ! found ) {
		return false;
	}

	return true;
}

export default {
	validateText, validateEmail, validateCoworkerDesk
}
