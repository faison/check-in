/**
 * Functions used to make submissions.
 *
 * @summary Functions used to make submissions.
 *
 * @since 1.0.0
 */

import data from './data.js';

let $ = window.jQuery;

/**
 * Submits visitor information to the server to be logged.
 *
 * @summary Submits visitor information to the server to be logged.
 *
 * @since 1.0.0
 * @access public
 *
 * @param {Object}   args     - The visitor information to log.
 * @param {Function} callback - The callback function for submission responses.
 */
function submitCheckIn( args, callback ) {
	let postData = {
		'visitor_name': args.yourName,
		'visitor_email': args.yourEmail,
		'visitor_desk': args.visiting
	};

	postData[ data.getNonceName() ] = data.getNonce();
	postData[ data.getSubmissionFlag() ] = 1;

	$.post( data.getSubmissionUrl(), postData, ( response ) => {
		if ( response.success ) {
			callback( true );
		}

		let errorInfo = {
			'code': response.data.error,
			'data': response.data.data
		};

		if ( 'invalid_meta_values' === errorInfo.code ) {
			let newData = [];

			//This is painful to look at and should be refactored to be less hardcoded at some time in the future
			if ( errorInfo.data.includes( 'visitor_name' ) ) {
				newData.push( 'yourName' );
			}

			if ( errorInfo.data.includes( 'visitor_email' ) ) {
				newData.push( 'yourEmail' );
			}

			if ( errorInfo.data.includes( 'visitor_desk' ) ) {
				newData.push( 'visiting' );
			}

			errorInfo.data = newData;
		}

		callback( false, errorInfo );
	} );
}

export default {
	submitCheckIn
}
