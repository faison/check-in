/**
 * Functions used for accessing data localized from WordPress.
 *
 * @summary Functions used for accessing data localized from WordPress.
 *
 * @since 1.0.0
 */

const data = window.check_in_data || {};

/**
 * Returns an array of Coworkers.
 *
 * @summary Returns an array of Coworkers.
 *
 * @since 1.0.0
 * @access public
 *
 * @returns {Object[]} An array of Coworkers.
 */
function getCoworkers() {
	if ( data.coworkers ) {
		return data.coworkers;
	}

	return [];
}

/**
 * Returns the nonce used for submissions.
 *
 * @summary Returns the nonce used for submissions.
 *
 * @since 1.0.0
 * @access public
 *
 * @returns {string} The nonce used for submissions.
 */
function getNonce() {
	if ( data.nonce ) {
		return data.nonce;
	}

	return '';
}

/**
 * Returns the nonce name used for submissions.
 *
 * @summary Returns the nonce name used for submissions.
 *
 * @since 1.0.0
 * @access public
 *
 * @returns {string} The nonce name used for submissions.
 */
function getNonceName() {
	if ( data.nonce_name ) {
		return data.nonce_name;
	}

	return '';
}

/**
 * Returns the submission flag used for submissions.
 *
 * @summary Returns the submission flag used for submissions.
 *
 * @since 1.0.0
 * @access public
 *
 * @returns {string} The submission flag used for submissions.
 */
function getSubmissionFlag() {
	if ( data.submission_flag ) {
		return data.submission_flag;
	}

	return '';
}

/**
 * Returns the submission URL used for submissions.
 *
 * @summary Returns the submission URL used for submissions.
 *
 * @since 1.0.0
 * @access public
 *
 * @returns {string} The submission URL used for submissions.
 */
function getSubmissionUrl() {
	if ( data.submission_url ) {
		return data.submission_url;
	}

	return '';
}

export default {
	getCoworkers, getNonce, getNonceName, getSubmissionFlag, getSubmissionUrl
}
