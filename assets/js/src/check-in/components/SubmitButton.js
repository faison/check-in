/**
 * The SubmitButton React Component and its property definitions.
 *
 * @summary The SubmitButton React Component and its property definitions.
 *
 * @since 1.0.0
 *
 * @module components/SubmitButton
 */

let React = window.React;

/**
 * The SubmitButton React Component.
 *
 * @summary The SubmitButton React Component.
 *
 * @since 1.0.0
 * @access public
 *
 * @extends React.Component
 */
export default class SubmitButton extends React.Component {
	/**
	 * Renders the Submit Button.
	 *
	 * @summary Renders the Submit Button.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {XML} The Submit button JSX.
	 */
	render() {
		return (<input type="submit" value={this.props.label} disabled={this.props.disabled} />);
	}
}

SubmitButton.propTypes = {
	label: React.PropTypes.string,
	disabled: React.PropTypes.bool
};
