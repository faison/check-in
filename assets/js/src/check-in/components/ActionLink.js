/**
 * The ActionLink React Component and its property definitions.
 *
 * @summary The ActionLink React Component and its property definitions.
 *
 * @since 1.0.0
 *
 * @module components/ActionLink
 */

let React = window.React;

/**
 * The ActionLink React Component.
 *
 * @summary The ActionLink React Component.
 *
 * @since 1.0.0
 * @access public
 *
 * @extends React.Component
 */
export default class ActionLink extends React.Component {
	/**
	 * Used to initiate the Action Link component, binding its context with its callback functions.
	 *
	 * @summary Used to initiate the Action Link component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {Object} props - The Component Props the ActionLink was initiated with.
	 */
	constructor( props ) {
		super( props );

		this.clickHandler = this.clickHandler.bind( this );
	}

	clickHandler( e ) {
		e.preventDefault();

		this.props.onClick();
	}

	/**
	 * Renders the Action Link.
	 *
	 * @summary Renders the Action Link.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {XML} The Action Link JSX.
	 */
	render() {
		return (
			<div className="action-link">
				<a href="#" disabled={this.props.disabled} onClick={this.clickHandler}>{this.props.label}</a>
			</div>
		);
	}
}

ActionLink.propTypes = {
	label: React.PropTypes.string,
	disabled: React.PropTypes.bool,
	onClick: React.PropTypes.func
};
