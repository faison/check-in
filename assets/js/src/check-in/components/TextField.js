/**
 * The TextField React Component and its property definitions.
 *
 * @summary The TextField React Component and its property definitions.
 *
 * @since 1.0.0
 *
 * @module components/TextField
 * @see components/FormField
 */

let React = window.React;

/**
 * The TextField React Component.
 *
 * @summary The TextField React Component.
 *
 * @since 1.0.0
 * @access public
 *
 * @extends React.Component
 */
export default class TextField extends React.Component {
	/**
	 * Used to initiate the Text Field component, binding its context with its callback functions.
	 *
	 * @summary Used to initiate the Text Field component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {Object} props - The Component Props the TextField was initiated with.
	 */
	constructor( props ) {
		super( props );

		this.onChangeHandler = this.onChangeHandler.bind( this );
	}

	/**
	 * Used to bubble the Text Field's new value up the Component hierarchy.
	 *
	 * @summary Used to bubble the Text Field's new value up the Component hierarchy.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {Object} e - The event object.
	 */
	onChangeHandler( e ) {
		const name = this.props.name;
		let value  = e.target.value;

		this.props.onChange( name, value );
	}

	/**
	 * Renders the Text Field.
	 *
	 * @summary Renders the Text Field.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {XML} The Text Field JSX.
	 */
	render() {
		return (
			<input type="text" id={this.props.name} value={this.props.value}
				onChange={this.onChangeHandler} disabled={this.props.disabled} />
		);
	}
}

TextField.propTypes = {
	name: React.PropTypes.string,
	value: React.PropTypes.string,
	onChange: React.PropTypes.func,
	disabled: React.PropTypes.bool
};
