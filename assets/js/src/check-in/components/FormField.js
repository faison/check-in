/**
 * The FormField React Component and its property definitions.
 *
 * @summary The FormField React Component and its property definitions.
 *
 * @since 1.0.0
 *
 * @module components/FormField
 *
 * @see components/TextField
 * @see components/SelectField
 */

import TextField from './TextField';
import SelectField from './SelectField';

let React = window.React;

/**
 * The FormField React Component.
 *
 * @summary The FormField React Component.
 *
 * @since 1.0.0
 * @access public
 *
 * @extends React.Component
 */
export default class FormField extends React.Component {
	/**
	 * Renders the Form Field. This renders a different field depending on the type provided.
	 *
	 * @summary Renders the Form Field.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {XML} The Form Field JSX.
	 */
	render() {
		const name     = this.props.name;
		const type     = this.props.type;
		const value    = this.props.value;
		const disabled = this.props.disabled;

		let required     = '';
		let errorMessage = '';
		let field        = '';

		if ( this.props.required ) {
			required = '*';
		}

		if ( ! this.props.valid ) {
			errorMessage = (<span className="checkin-error">{this.props.errorMessage}</span>);
		}

		if ( 'text' === type ) {
			field = (<TextField name={name} value={value} onChange={this.props.onChange} disabled={disabled} />);
		} else if ( 'select' === type ) {
			field = (
				<SelectField name={name} value={value} options={this.props.options}
							onChange={this.props.onChange} disabled={disabled} />
			);
		}

		return (
			<div className="form-field">
				<label htmlFor={name}>
					{this.props.label} {required} {errorMessage}
				</label>
				{field}
			</div>
		);
	}
}

FormField.propTypes = {
	name: React.PropTypes.string,
	type: React.PropTypes.string,
	value: React.PropTypes.string,
	required: React.PropTypes.bool,
	errorMessage: React.PropTypes.string,
	valid: React.PropTypes.bool,
	options: React.PropTypes.array,
	label: React.PropTypes.string,
	onChange: React.PropTypes.func,
	disabled: React.PropTypes.bool
};
