/**
 * The SelectField React Component and its property definitions.
 *
 * @summary The SelectField React Component and its property definitions.
 *
 * @since 1.0.0
 *
 * @module components/SelectField
 * @see components/FormField
 */

let React = window.React;

/**
 * The SelectField React Component.
 *
 * @summary The SelectField React Component.
 *
 * @since 1.0.0
 * @access public
 *
 * @extends React.Component
 */
export default class SelectField extends React.Component {
	/**
	 * Used to initiate the Select Field component, binding its context with its callback functions.
	 *
	 * @summary Used to initiate the Select Field component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {Object} props - The Component Props the SelectField was initiated with.
	 */
	constructor( props ) {
		super( props );

		this.onChangeHandler = this.onChangeHandler.bind( this );
	}

	/**
	 * Used to bubble the Select Field's new value up the Component hierarchy.
	 *
	 * @summary Used to bubble the Select Field's new value up the Component hierarchy.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {Object} e - The event object.
	 */
	onChangeHandler( e ) {
		const name = this.props.name;
		let value  = e.target.value;

		this.props.onChange( name, value );
	}

	/**
	 * Renders the Select Field.
	 *
	 * @summary Renders the Select Field.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {XML} The Select Field JSX.
	 */
	render() {
		const options = this.props.options.map(( option ) =>
			<option value={option.desk} key={option.desk}>{option.name}</option>
		);

		return (
			<select id={this.props.name} value={this.props.value} onChange={this.onChangeHandler} disabled={this.props.disabled}>
				<option value=""></option>
				{options}
			</select>
		);
	}
}

SelectField.propTypes = {
	options: React.PropTypes.array,
	name: React.PropTypes.string,
	value: React.PropTypes.string,
	onChange: React.PropTypes.func,
	disabled: React.PropTypes.bool
};
