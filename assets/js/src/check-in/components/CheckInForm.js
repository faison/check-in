/**
 * The CheckInForm React Component and its property definitions.
 *
 * @summary The CheckInForm React Component and its property definitions.
 *
 * @since 1.0.0
 *
 * @module components/CheckInForm
 */

import data from '../data.js';
import validation from '../validation.js';
import submission from '../submission';
import ConfirmationMessage from './ConfirmationMessage';
import SubmitButton from './SubmitButton';
import ActionLink from './ActionLink';
import FormField from './FormField';

let React = window.React;

/**
 * The CheckInForm React Component.
 *
 * @summary The CheckInForm React Component.
 *
 * @since 1.0.0
 * @access public
 *
 * @extends React.Component
 */
export default class CheckInForm extends React.Component {
	/**
	 * Used to initiate the Check-in Form component, binding its context with its callback functions
	 * and setting its initial state.
	 *
	 * @summary Used to initiate the Check-in Form component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {Object} props - The Component Props the CheckInForm was initiated with.
	 */
	constructor( props ) {
		super( props );

		this.state = {
			yourName: '',
			yourEmail: '',
			visiting: '',
			loading: false,
			showErrors: false,
			generalError: '',
			showConfirmation: this.props.showConfirmation
		};

		this.fieldChangeHandler        = this.fieldChangeHandler.bind( this );
		this.submissionHandler         = this.submissionHandler.bind( this );
		this.confirmationTimerFinished = this.confirmationTimerFinished.bind( this );
		this.submissionCallback        = this.submissionCallback.bind( this );
	}

	/**
	 * Callback function that updates the Check-in Form's value state for a field..
	 *
	 * @summary Callback function that updates the Check-in Form's value state for a field.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {string}        name  - The field name.
	 * @param {string|number} value - The field value.
	 */
	fieldChangeHandler( name, value ) {
		let newState = {};

		newState[ name ] = value;

		this.setState( newState );
	}

	/**
	 * Handles a form submission, showing errors if there are any or showing the Confirmation Message.
	 * The Confirmation Message will only show if the form data is successfully submitted to the site.
	 * Otherwise, show a server error.
	 *
	 * @summary Handles a form submission, showing errors if there are any or showing the Confirmation Message.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {Object} e - The event object.
	 */
	submissionHandler( e ) {
		e.preventDefault();

		let nameValid     = validation.validateText( this.state.yourName );
		let emailValid    = validation.validateEmail( this.state.yourEmail );
		let visitingValid = validation.validateCoworkerDesk( this.state.visiting );

		if ( ! ( nameValid && emailValid && visitingValid ) ) {
			this.setState({
				showErrors: true
			});

			return;
		}

		this.setState({
			loading: true
		});

		submission.submitCheckIn({
			yourName: this.state.yourName,
			yourEmail: this.state.yourEmail,
			visiting: this.state.visiting
		}, this.submissionCallback );
	}

	/**
	 * The callback function for the ConfirmationMessage's timer trigger. Resets the forms.
	 *
	 * @summary The callback function for the ConfirmationMessage's timer trigger.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	confirmationTimerFinished() {
		this.setState({
			yourName: '',
			yourEmail: '',
			visiting: '',
			loading: false,
			showErrors: false,
			generalError: '',
			showConfirmation: false
		});
	}

	/**
	 * The callback function for a submission response.
	 *
	 * @summary The callback function for a submission response.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {bool}   success - Whether or not the submission was a success.
	 * @param {Object} context - Context for the success or failure
	 */
	submissionCallback( success, context ) {
		if ( success ) {
			this.setState({
				showConfirmation: true
			});

			return;
		}

		let newState = {
			loading: false,
			showErrors: true
		};

		if ( 'invalid_nonce' === context.code ) {
			newState.generalError = 'Invalid Nonce, please refresh page';
		} else if ( 'db_add_log' === context.code ) {
			newState.generalError = 'Database connection error, please try again shortly';
		} else if ( 'unexpected_add_log' === context.code ) {
			newState.generalError = 'An unexpected error occurred , please try again shortly';
		} else if ( 'visitor_already_checked_in' === context.code ) {
			newState.generalError = 'We\'re sorry, but you can only check in once per day';
		} else if ( 'invalid_meta_values' === context.code ) {
			newState.generalError = 'Please check your field values and refresh if there are still issues';
		}

		this.setState( newState );
	}

	/**
	 * Renders the Check-in Form. This switches between a Confirmation message and the actual form.
	 *
	 * @summary Renders the Check-in Form.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {XML} The Check-in Form.
	 */
	render() {
		if ( this.state.showConfirmation ) {
			return (
				<div>
					<ConfirmationMessage secondsLeft={3} timerFinished={this.confirmationTimerFinished} />
				</div>
			);
		}

		let formClass     = 'check-in-form';
		let generalNote   = (<div className="general-note"></div>);
		let nameValid     = true;
		let emailValid    = true;
		let visitingValid = true;
		let submitText    = 'Check In';
		let actionLink    = '';

		const loading = this.state.loading;

		if ( loading ) {
			formClass  += ' loading';
			submitText  = 'Checking in...';
		} else {
			actionLink = (<ActionLink label="Reset" disabled={loading} onClick={this.confirmationTimerFinished} />);
		}

		if ( this.state.showErrors ) {
			nameValid     = validation.validateText( this.state.yourName );
			emailValid    = validation.validateEmail( this.state.yourEmail );
			visitingValid = validation.validateCoworkerDesk( this.state.visiting );

			if ( this.state.generalError ) {
				generalNote = (<div className="general-note checkin-error">{this.state.generalError}</div>);
			}
		}

		return (
			<form className={formClass} onSubmit={this.submissionHandler}>
				{generalNote}
				<FormField name="yourName" label="Your Name" type="text" value={this.state.yourName}
							required={true} errorMessage="Please enter your name" onChange={this.fieldChangeHandler}
							valid={nameValid} disabled={loading} />
				<FormField name="yourEmail" label="Your Email" type="text" value={this.state.yourEmail}
							required={true} errorMessage="Please enter a valid email" onChange={this.fieldChangeHandler}
							valid={emailValid} disabled={loading} />
				<FormField name="visiting" label="Who are you visiting?" type="select"
							required={true} errorMessage="Please select an option"
							value={this.state.visiting} options={data.getCoworkers()} onChange={this.fieldChangeHandler}
							valid={visitingValid} disabled={loading} />

				<SubmitButton label={submitText} disabled={loading} />
				{actionLink}
			</form>
		);
	}
}

CheckInForm.propTypes = {
	showConfirmation: React.PropTypes.bool
};

CheckInForm.defaultProps = {
	showConfirmation: false
};
