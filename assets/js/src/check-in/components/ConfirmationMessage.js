/**
 * The ConfirmationMessage React Component and its property definitions.
 *
 * @summary The ConfirmationMessage React Component and its property definitions.
 *
 * @since 1.0.0
 *
 * @module components/ConfirmationMessage
 */

let React = window.React;

/**
 * The ConfirmationMessage React Component.
 *
 * @summary The ConfirmationMessage React Component.
 *
 * @since 1.0.0
 * @access public
 *
 * @extends React.Component
 */
export default class ConfirmationMessage extends React.Component {
	/**
	 * Used to initiate the Confirmation Message component, binding its context with its callback functions
	 * and setting its initial state.
	 *
	 * @summary Used to initiate the Confirmation Message component.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param {Object} props - The Component Props the ConfirmationMessage was initiated with.
	 */
	constructor( props ) {
		super( props );

		this.state = {
			secondsLeft: this.props.secondsLeft
		};

		this.tick = this.tick.bind( this );
	}

	/**
	 * Starts the interval used for the Confirmation Message's secondsLeft counter.
	 *
	 * @summary Starts the interval used for the Confirmation Message's secondsLeft counter.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	componentDidMount() {
		this.intervalId = setInterval(
			() => this.tick(),
			1000
		);
	}

	/**
	 * Clears out the interval used for the Confirmation Message's secondsLeft counter.
	 *
	 * @summary Clears out the interval used for the Confirmation Message's secondsLeft counter.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	componentWillUnmount() {
		clearInterval( this.intervalId );
	}

	/**
	 * Decrements the Confirmation Message's secondsLeft counter, triggering if zero is reached.
	 *
	 * @summary Decrements the Confirmation Message's secondsLeft counter, triggering if zero is reached.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	tick() {
		this.setState(( prevState ) => ({
			secondsLeft: prevState.secondsLeft - 1
		}));

		if ( 0 >= this.state.secondsLeft ) {
			this.props.timerFinished();
		}
	}

	/**
	 * Renders the Confirmation Message.
	 *
	 * @summary Renders the Confirmation Message.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @returns {XML} The Confirmation Message JSX.
	 */
	render() {
		return (
			<div>
				<p><strong>Thank you for checking in!</strong></p>
				<p><em>This form will reset in {this.state.secondsLeft} seconds</em></p>
			</div>
		);
	}
}

ConfirmationMessage.propTypes = {
	secondsLeft: React.PropTypes.number,
	timerFinished: React.PropTypes.func
};
