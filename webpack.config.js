var path = require( 'path' );

module.exports = [
	{
		name: 'public',
		entry: './assets/js/src/check-in.js',
		output: {
			path: path.join( __dirname, 'assets/js' ),
			filename: 'check-in.js'
		},
		module: {
			preLoaders: [
				{
					test: path.join( __dirname, 'assets/js/src' ),
					loader: 'eslint-loader'
				}
			],
			loaders: [
				{
					test: path.join( __dirname, 'assets/js/src' ),
					loader: 'babel-loader'
				}
			]
		}
	}
];
