<?php
/**
 * The main plugin file responsible for starting up the "Check-in" plugin.
 *
 * @package Check_In
 * @since 1.0.0
 *
 * @wordpress-plugin
 * Plugin Name: Visitor Check-in
 * Plugin URI:  https://bitbucket.org/faison/check-in
 * Description: Add visitor check-in to your office and have logs of all visits.
 * Version:     1.0.0
 * Author:      Faison Zutavern
 * Author URI:  http://faisonz.net
 */

namespace Check_In;

define( 'CHECK_IN_VERSION', '1.0.0' );
define( 'CHECK_IN_PATH', dirname( __FILE__ ) . '/' );
define( 'CHECK_IN_URL', plugin_dir_url( __FILE__ ) );

require_once 'includes/setup.php';

require_once 'includes/coworkers/coworkers.php';
require_once 'includes/coworkers/remote-pull.php';
require_once 'includes/coworkers/settings.php';

require_once 'includes/visitor-log/model.php';
require_once 'includes/visitor-log/post-type.php';
require_once 'includes/visitor-log/post-meta.php';
require_once 'includes/visitor-log/list-table.php';

require_once 'includes/endpoints/model.php';
require_once 'includes/endpoints/rewrites.php';
require_once 'includes/endpoints/visit-endpoint.php';
require_once 'includes/endpoints/submission-endpoint.php';

setup();

register_activation_hook( __FILE__, __NAMESPACE__ . '\activate' );
