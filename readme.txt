=== Visitor Check-in ===
Contributors: faison
Requires at least: 4.7
Tested up to: 4.7
Stable tag: 1.0.0

Add visitor check-in to your office and have logs of all visits.
