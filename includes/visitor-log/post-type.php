<?php
/**
 * Visitor Log Post Type Registration and Creation functions.
 *
 * @package Check_In\Visitor_Log
 * @since 1.0.0
 */

namespace Check_In\Visitor_Log;

/**
 * Registers the Visitor Log post type.
 *
 * @since 1.0.0
 */
function register_visitor_log_post_type() {
	$labels = array(
		'name'               => esc_html__( 'Visitor Log', 'check_in'),
		'singular_name'      => esc_html__( 'Visitor Log', 'check_in' ),
		'add_new'            => '',
		'add_new_item'       => '',
		'edit_item'          => esc_html__( 'View visitor log', 'check_in' ),
		'new_item'           => '',
		'all_items'          => esc_html__( 'All Visitor Logs', 'check_in' ),
		'view_item'          => '',
		'search_items'       => esc_html__( 'Search Visitor Logs', 'check_in' ),
		'not_found'          => esc_html__( 'No Visitor Log found', 'check_in' ),
		'not_found_in_trash' => esc_html__( 'No Visitor Log found in Trash', 'check_in' ),
		'parent_item_colon'  => '',
		'menu_name'          => esc_html__( 'Visitor Logs', 'check_in' ),
	);

	$registration_args = array(
		'labels'          => $labels,
		'public'          => false,
		'description'     => esc_html__( 'Visitor Check-in Logs', 'mtf_yw' ),
		'capability_type' => 'post',
		'capabilities'    => array(
			'create_posts' => 'do_not_allow',
		),
		'map_meta_cap'    => true,
		'hierarchical'    => false,
		'menu_position'   => 21,
		'menu_icon'       => 'dashicons-editor-ul',
		'show_ui'         => true,
		'has_archive'     => false,
		'rewrite'         => false,
		'supports'        => array(
			'title',
		),
	);

	register_post_type( get_post_type_name(), $registration_args );
}

add_action( 'check_in_init', __NAMESPACE__ . '\register_visitor_log_post_type' );

/**
 * Adds a Visitor Log with the included meta data.
 *
 * @since 1.0.0
 * @see get_meta_defs() for the valid meta keys.
 *
 * @param array $args The meta values to include with the log.
 *
 * @return int|\WP_Error The post id for the added log, or a \WP_Error if not created.
 */
function add_log( $args ) {
	$meta_defs    = get_meta_defs();
	$valid_meta   = array();
	$invalid_meta = array();

	foreach ( $meta_defs as $meta_key => $meta_def ) {
		$value = '';

		if ( ! empty( $args[ $meta_key ] ) ) {
			$value = call_user_func( $meta_def['sanitize_callback'], $args[ $meta_key ] );
		}

		if ( empty( $value ) ) {
			$invalid_meta[] = $meta_key;
			$value = '';
		}

		$valid_meta[ $meta_key ] = $value;
	}

	if ( ! empty( $invalid_meta ) ) {
		return new \WP_Error( 'invalid_meta_values', esc_html__( 'Required meta values invalid', 'check_in' ), $invalid_meta );
	}

	$args = array(
		'post_type' => get_post_type_name(),
		'post_status' => 'publish',
		'post_title'  => $valid_meta['visitor_name'],
		'meta_input'  => $valid_meta,
	);

	if ( has_visitor_checked_in_today( $valid_meta['visitor_email'] ) ) {
		return new \WP_Error(
			'visitor_already_checked_in',
			esc_html__( 'Visitor cannot check-in more than once a day', 'check_in' ),
			$valid_meta['visitor_email']
		);
	}

	$log_id = wp_insert_post( $args, true );

	if ( is_wp_error( $log_id ) ) {
		if ( 'db_insert_error' === $log_id->get_error_code() ) {
			return new \WP_Error( 'db_add_log', esc_html__( 'Database error', 'check_in' ) );
		}

		return new \WP_Error( 'unexpected_add_log', esc_html__( 'Unexpected error', 'check_in' ) );
	}

	/**
	 * Fires after a Visitor Log is created.
	 *
	 * @since 1.0.0
	 *
	 * @param int   $log_id     The Post ID of the new Visitor Log.
	 * @param array $valid_meta The Visitor Log meta.
	 */
	do_action( 'check_in_log_added', $log_id, $valid_meta );

	return $log_id;
}

/**
 * Checks if the visitor, with the provided email, has checked-in today.
 *
 * @since 1.0.0
 *
 * @param string $visitor_email The visitor's email address.
 *
 * @return bool True if the visitor has checked-in today, otherwise false.
 */
function has_visitor_checked_in_today( $visitor_email ) {
	$visitor_email = sanitize_email( $visitor_email );

	// Can't look for a visitor without an email.
	if ( empty( $visitor_email ) ) {
		return false;
	}

	$date  = getdate();
	$query = new \WP_Query( array(
		'post_type'      => get_post_type_name(),
		'post_status'    => 'publish',
		'posts_per_page' => 1,
		'no_found_rows'  => true,
		'date_query'     => array(
			array(
				'year'  => $date['year'],
				'month' => $date['mon'],
				'day'   => $date['mday'],
			),
		),
		'meta_query'     => array(
			array(
				'key'   => 'visitor_email',
				'value' => $visitor_email,
			)
		)
	) );

	if ( ! $query->have_posts() ) {
		return false;
	}

	return true;
}
