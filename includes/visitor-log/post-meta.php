<?php
/**
 * Visitor Log Post Meta Registration and Sanitization code.
 *
 * @package Check_In\Visitor_Log
 * @since 1.0.0
 */

namespace Check_In\Visitor_Log;

/**
 * Registers the Visitor Log Meta with WordPress.
 *
 * @since 1.0.0
 */
function register_visitor_log_meta() {
	$meta_defs = get_meta_defs();

	foreach ( $meta_defs as $meta_key => $meta_def ) {
		register_meta( 'post', $meta_key, $meta_def );
	}
}

add_action( 'check_in_init', __NAMESPACE__ . '\register_visitor_log_post_type' );
