<?php
/**
 * Visitor Log Post List Table functions for modify the columns and content.
 *
 * @package Check_In\Visitor_Log
 * @since 1.0.0
 */

namespace Check_In\Visitor_Log;

/**
 * Filters the Visitor Log list table row actions, removing the edit and quick edit actions.
 *
 * @since 1.0.0
 *
 * @param array    $actions An array of row action links.
 * @param \WP_Post $post    The post object.
 *
 * @return array The visitor log row actions with the edit and quick edit actions removed.
 */
function filter_visitor_log_row_actions( $actions, $post ) {
	if ( empty( $actions ) || empty( $post ) ) {
		return $actions;
	}

	if ( get_post_type_name() !== $post->post_type ) {
		return $actions;
	}

	// Unset the edit and quick edit actions
	unset( $actions['edit'] );
	unset( $actions['inline hide-if-no-js'] );

	return $actions;
}

add_filter( 'post_row_actions', __NAMESPACE__ . '\filter_visitor_log_row_actions', 10, 2 );

/**
 * Filters the Visitor Log list table bulk actions, removing the edit action.
 *
 * @since 1.0.0
 *
 * @param array $actions An array of the available bulk actions.
 *
 * @return array The visitor log bulk actions with the edit action removed.
 */
function filter_visitor_log_bulk_actions( $actions ) {
	if ( ! is_array( $actions ) || empty( $actions ) ) {
		return $actions;
	}

	unset( $actions['edit'] );

	return $actions;
}

add_filter( 'bulk_actions-edit-' . get_post_type_name(), __NAMESPACE__ . '\filter_visitor_log_bulk_actions' );

/**
 * Adds columns for the Visitor Log Meta in the Visitor Log List Table.
 *
 * @since 1.0.0
 *
 * @param array $posts_columns An array of column names.
 *
 * @return array The array of column names visible in the Visitor Log List Table.
 */
function filter_visitor_log_post_columns( $posts_columns ) {
	if ( ! is_array( $posts_columns ) || empty( $posts_columns ) ) {
		return $posts_columns;
	}

	unset( $posts_columns['date'] );
	unset( $posts_columns['title'] );

	// Custom post date columns so we can fully control the date output
	$posts_columns['visitor_date'] = esc_html__( 'Date', 'check_in' );

	$meta_defs = get_meta_defs();

	foreach ( $meta_defs as $meta_key => $meta_def ) {
		$posts_columns[ $meta_key ] = $meta_def['short_description'];
	}

	return $posts_columns;
}

add_filter( 'manage_' . get_post_type_name() . '_posts_columns', __NAMESPACE__ . '\filter_visitor_log_post_columns' );

/**
 * Renders meta values for the Visitor Log meta columns in the Visitor Log List Table.
 *
 * @since 1.0.0
 *
 * @param string $column_name The name of the column to display.
 * @param int    $post_id     The current post ID.
 */
function display_visitor_log_meta_for_list_table_column( $column_name, $post_id ) {
	$meta_defs = get_meta_defs();

	if ( ! array_key_exists( $column_name, $meta_defs ) ) {
		return;
	}

	$meta_def = $meta_defs[ $column_name ];

	// Always pass false for $single to ensure we're working with an array of values
	$values = get_post_meta( $post_id, $column_name, false );

	if ( empty( $values ) ) {
		echo esc_html__( '&mdash;', 'check_in' );
	}

	// Do not display user submitted data unless the meta definition has a valid output callback defined
	if ( ! is_callable( $meta_def['output_callback'] ) ) {
		return;
	}

	// Output sanitization happens here, make sure
	$outputs = array_map( $meta_def['output_callback'], $values );
	$outputs = array_filter( $outputs );

	echo implode( ', ', $outputs );
}

add_action(
	'manage_' . get_post_type_name() . '_posts_custom_column',
	__NAMESPACE__ . '\display_visitor_log_meta_for_list_table_column',
	10, 2
);

/**
 * Renders the post date for the Visitor Log meta columns in the Visitor Log List Table.
 *
 * @since 1.0.0
 *
 * @param string $column_name The name of the column to display.
 * @param int    $post_id     The current post ID.
 */
function display_visitor_log_date_for_list_table_column( $column_name, $post_id ) {
	if ( 'visitor_date' !== $column_name ) {
		return;
	}

	$post = get_post( $post_id );

	if ( '0000-00-00 00:00:00' === $post->post_date ) {
		// This shouldn't happen, but add some error output in case
		echo esc_html__( 'Error: No date!', 'check_in' );
		return;
	}

	$format   = get_option( 'date_format' ) . ' ' . get_option( 'time_format' );
	$the_time = get_the_time( $format, $post );

	echo esc_html( $the_time );
}

add_action(
	'manage_' . get_post_type_name() . '_posts_custom_column',
	__NAMESPACE__ . '\display_visitor_log_date_for_list_table_column',
	10, 2
);

/**
 * Makes the Meta Field columns sortable in the Visitor Log List Table.
 *
 * @since 1.0.0
 *
 * @param array $sortable_columns An array of sortable columns.
 *
 * @return array The sortable columns, including the Visitor Log Meta Field columns.
 */
function filter_sortable_columns( $sortable_columns ) {
	if ( ! is_array( $sortable_columns ) || empty( $sortable_columns ) ) {
		return $sortable_columns;
	}

	$meta_defs      = get_meta_defs();
	$custom_columns = array(
		'visitor_date' => array( 'date', true ),
	);

	foreach ( $meta_defs as $meta_key => $meta_def ) {
		$custom_columns[ $meta_key ] = $meta_key;
	}

	$sortable_columns = array_merge( $sortable_columns, $custom_columns );

	return $sortable_columns;
}

add_filter( 'manage_edit-' . get_post_type_name() . '_sortable_columns', __NAMESPACE__ . '\filter_sortable_columns' );

/**
 * Shows the Desk Filter for the Visitor Log List Table.
 *
 * @param string $post_type The post type slug.
 */
function show_desk_filter( $post_type ) {
	if ( get_post_type_name() !== $post_type ) {
		return;
	}

	$coworkers = \Check_In\Coworkers\get_coworkers();
	$desk      = filter_input( INPUT_GET, 'visitor_desk', FILTER_CALLBACK, array(
		'options' => '\Check_In\Coworkers\sanitize_desk_number',
	) );

	if ( empty( $desk ) ) {
		$desk = 0;
	}

	?>
	<label for="filter-by-desk" class="screen-reader-text"><?php _e( 'Filter by desk' ); ?></label>
	<select name="visitor_desk" id="filter-by-desk">
		<option <?php selected( $desk, 0 ); ?> value="0"><?php _e( 'All desks' ); ?></option>
		<?php foreach ( $coworkers as $coworker ) {
			printf(
				'<option%1$s value="%2$d">%3$s</option>',
				selected( $desk, $coworker['desk'], false ),
				intval( $coworker['desk'] ),
				esc_html( $coworker['name'] )
			);
		} ?>
	</select>
	<?php
}

add_action( 'restrict_manage_posts', __NAMESPACE__ . '\show_desk_filter' );

/**
 * Updates the Visitor Log List Table Query to handle sorting and filtering on Visitor Log Meta Fields.
 *
 * This also changes how search queries are setup.
 *
 * @since 1.0.0
 *
 * @param \WP_Query &$query The WP_Query instance (passed by reference).
 */
function update_list_table_query( $query ) {
	if ( ! is_admin() ) {
		return;
	}

	$screen = get_current_screen();

	if ( empty( $screen ) || 'edit-' . get_post_type_name() !== $screen->id ) {
		return;
	}

	$meta_defs = get_meta_defs();
	$orderby   = $query->get( 'orderby' );

	// Order by one of the meta keys
	if ( ! empty( $orderby ) && 'date' !== $orderby && array_key_exists( $orderby, $meta_defs ) ) {
		$meta_def = $meta_defs[ $orderby ];

		$query->set( 'meta_key', $orderby );

		if ( in_array( $meta_def['type'], array( 'number', 'integer' ) ) ) {
			$query->set( 'orderby', 'meta_value_num' );
		} else {
			$query->set( 'orderby', 'meta_value' );
		}
	}

	$desk = filter_input( INPUT_GET, 'visitor_desk', FILTER_CALLBACK, array(
		'options' => '\Check_In\Coworkers\sanitize_desk_number',
	) );

	$meta_query = array();

	if ( ! empty( $desk ) ) {
		$meta_query = array(
			'relation' => 'AND',
			array(
				'key'     => 'visitor_desk',
				'value'   => $desk,
				'compare' => '=',
				'type'    => 'numeric',
			)
		);
	}

	// Search the meta fields also
	if ( $query->is_search() ) {
		$search_query = array(
			'relation' => 'OR',
		);

		foreach ( $meta_defs as $meta_key => $meta_def ) {
			$value = $query->get( 's' );

			// Only allow filtering by the desk, don't search on it
			if ( 'visitor_desk' === $meta_key ) {
				continue;
			}

			$search_query[] = array(
				'key'     => $meta_key,
				'value'   => $value,
				'compare' => 'LIKE',
			);
		}

		$query->set( 's', '' );

		if ( empty( $meta_query ) ) {
			$meta_query = $search_query;
		} else {
			$meta_query[] = $search_query;
		}
	}

	if ( ! empty( $meta_query ) ) {
		$query->set( 'meta_query', $meta_query );
	}
}

add_action( 'pre_get_posts', __NAMESPACE__ . '\update_list_table_query' );

/**
 * Makes sure the get_search_query() function returns a search value for the Visitor Log list table.
 *
 * @param string $search Contents of the search query variable.
 *
 * @return string The contents of the search query variable, reset for the Visitor Log list table.
 */
function filter_search_query_value( $search ) {
	if ( ! is_admin() ) {
		return $search;
	}

	$screen = get_current_screen();

	if ( empty( $screen ) || 'edit-' . get_post_type_name() !== $screen->id ) {
		return $search;
	}

	$search_var = filter_input( INPUT_GET, 's', FILTER_CALLBACK, array(
		'options' => 'sanitize_text_field',
	) );

	if ( empty( $search_var ) ) {
		return $search;
	}

	return $search_var;
}

add_filter( 'get_search_query', __NAMESPACE__ . '\filter_search_query_value' );
