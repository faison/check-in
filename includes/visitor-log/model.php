<?php
/**
 * Visitor Log Model functions.
 *
 * This contains functions that return things like the post type name, meta keys, etc.
 *
 * @package Check_In\Visitor_Log
 * @since 1.0.0
 */

namespace Check_In\Visitor_Log;

/**
 * Returns the post type name.
 *
 * @since 1.0.0
 *
 * @return string The post type name.
 */
function get_post_type_name() {
	return 'visitor_log';
}

/**
 * Returns the Visitor Log Meta definitions.
 *
 * This includes data used by register_meta
 *
 * @since 1.0.0
 * @see register_meta for the definition of the args included here.
 *
 * @return array The Visitor Log Meta definitions.
 */
function get_meta_defs() {
	return array(
		'visitor_name'  => array(
			'sanitize_callback' => 'sanitize_text_field',
			'output_callback'   => 'esc_html',
			'auth_callback'     => '__return_true',
			'type'              => 'string',
			'description'       => esc_html__( 'The visitor\'s name', 'check_in'),
			'short_description' => esc_html__( 'Visitor', 'check_in' ),
			'single'            => true,
		),
		'visitor_email' => array(
			'sanitize_callback' => 'sanitize_email',
			'output_callback'   => 'sanitize_email',
			'auth_callback'     => '__return_true',
			'type'              => 'email',
			'description'       => esc_html__( 'The visitor\'s email', 'check_in'),
			'short_description' => esc_html__( 'Email', 'check_in' ),
			'single'            => true,
		),
		'visitor_desk'  => array(
			'sanitize_callback' => '\Check_In\Coworkers\sanitize_desk_number',
			'output_callback'   => '\Check_In\Coworkers\get_coworker_string',
			'auth_callback'     => '__return_true',
			'type'              => 'integer',
			'description'       => esc_html__( 'The desk the visitor visited', 'check_in'),
			'short_description' => esc_html__( 'Desk', 'check_in' ),
			'single'            => true,
		),
	);
}
