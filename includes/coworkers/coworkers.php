<?php
/**
 * Coworker related code.
 *
 * @package Check_In\Coworkers
 * @since 1.0.0
 */

namespace Check_In\Coworkers;

/**
 * Retrieves a coworker by desk number.
 *
 * @param int $desk The number to get a coworker by.
 *
 * @return array|string An associative array of the coworker info, empty string if not found.
 */
function get_coworker_by_desk( $desk ) {
	$desk = intval( $desk );

	if ( 0 > $desk ) {
		return '';
	}

	$coworkers      = get_coworkers();
	$valid_desks    = wp_list_pluck( $coworkers, 'desk' );
	$coworker_index = array_search( $desk, $valid_desks );

	if ( false === $coworker_index ) {
		return '';
	}

	return $coworkers[ $coworker_index ];
}

/**
 * Sanitizes and validates a number as a desk number.
 *
 * @since 1.0.0
 *
 * @param int $desk The number to sanitize/validate as a desk number.
 *
 * @return int|string The valid desk number, or an empty string if invalid.
 */
function sanitize_desk_number( $desk ) {
	$desk     = intval( $desk );
	$coworker = get_coworker_by_desk( $desk );

	if ( empty( $coworker ) ) {
		return '';
	}

	return $coworker['desk'];
}

/**
 * Returns the coworker display string for the specified desk.
 *
 * @since 1.0.0
 *
 * @param int        $desk             The desk number to return the string for.
 * @param bool|true  $include_desk_num Whether or not to show the desk number with the coworker's name.
 *
 * @return string The coworker string for the specified desk, empty string if desk not found.
 */
function get_coworker_string( $desk, $include_desk_num = true ) {
	$coworker = get_coworker_by_desk( $desk );

	if ( empty( $coworker ) ) {
		return '';
	}

	if ( ! $include_desk_num ) {
		return esc_html( $coworker['name'] );
	}

	return sprintf(
			'(%1$d) %2$s',
			intval( $coworker['desk'] ),
			esc_html( $coworker['name'] )
	);
}
