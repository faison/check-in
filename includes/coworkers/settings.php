<?php
/**
 * Functionality for storing and retrieving coworkers from options.
 *
 * @package Check_In\Coworkers
 * @since 1.0.0
 */

namespace Check_In\Coworkers;

/**
 * Returns the Coworkers Option name.
 *
 * @since 1.0.0
 *
 * @return string The Coworkers Option name.
 */
function get_coworkers_option_name() {
	return 'check_in_coworkers';
}

/**
 * Returns the Coworkers Warning Option name.
 *
 * @since 1.0.0
 *
 * @return string The Coworkers Warning Option name.
 */
function get_coworkers_warning_option_name() {
	return 'check_in_coworkers_warning';
}

/**
 * Returns an array of coworkers that can be visited.
 *
 * @since 1.0.0
 *
 * @return array An array of coworkers that can be visited.
 */
function get_coworkers() {
	return get_option( get_coworkers_option_name(), array() );
}

/**
 * Updates the coworkers option with the coworkers pulled from the remote source.
 *
 * @since 1.0.0
 */
function update_stored_coworkers() {
	$coworkers = pull_coworkers_from_source();

	if ( is_wp_error( $coworkers ) ) {
		update_option( get_coworkers_warning_option_name(), $coworkers->get_error_message() );
		return;
	}

	delete_option( get_coworkers_warning_option_name() );

	update_option( get_coworkers_option_name(), $coworkers );
}

/**
 * Shows the error message that popped up when pulling the coworkers from the remote source.
 *
 * @since 1.0.0
 */
function show_coworker_pull_warning() {
	$warning = get_option( get_coworkers_warning_option_name(), '' );

	if ( empty( $warning ) ) {
		return;
	}

	?>
	<div class="notice notice-error">
		<p>
			<strong><?php echo esc_html__( 'Visitor Check-in Coworker Pull Error:', 'check_in' ); ?></strong>
			<?php echo esc_html( $warning ); ?>
		</p>
	</div>
	<?php
}

add_action( 'admin_notices', __NAMESPACE__ . '\show_coworker_pull_warning' );
