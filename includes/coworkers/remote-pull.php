<?php
/**
 * Functionality for pulling coworkers from a remote source.
 *
 * @package Check_In\Coworkers
 * @since 1.0.0
 */

namespace Check_In\Coworkers;

/**
 * Returns the source URL for the coworker data.
 *
 * @since 1.0.0
 *
 * @return string The source URL for the coworker data.
 */
function get_coworker_source_url() {
	return 'https://gist.githubusercontent.com/jjeaton/21f04d41287119926eb4/raw/4121417bda0860f662d471d1d22b934a0af56eca/coworkers.json';
}

/**
 * Retrieves the coworker data from the remote source and returns it.
 *
 * @since 1.0.0
 *
 * @return array|\WP_Error The coworker data.
 */
function pull_coworkers_from_source() {
	$response = wp_remote_get( get_coworker_source_url() );

	if ( is_wp_error( $response ) ) {
		return $response;
	}

	$response_code = wp_remote_retrieve_response_code( $response );

	if ( 200 !== $response_code ) {
		return new \WP_Error( 'non_200_response', esc_html__( 'Request returned a non 200 response code', 'check_in' ), array(
			'response_code' => $response_code
		) );
	}

	$response_body = wp_remote_retrieve_body( $response );
	$coworkers     = json_decode( $response_body, true );

	if ( empty( $coworkers ) ) {
		return new \WP_Error( 'empty_response', esc_html__( 'Empty Response body', 'check_in' ) );
	}

	return $coworkers;
}
