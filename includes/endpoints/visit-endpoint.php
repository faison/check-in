<?php
/**
 * The visit endpoint functions.
 *
 * @package Check_In\Endpoints
 * @since 1.0.0
 */

namespace Check_In\Endpoints;

/**
 * Returns the URL for the visit endpoint.
 *
 * @since 1.0.0
 *
 * @return string The URL for the visit endpoint.
 */
function get_visit_url() {
	return get_home_url() . '/' . get_visit_slug() . '/';
}

/**
 * Makes sure the correct template is used for the visit endpoint.
 *
 * @since 1.0.0
 *
 * @param string $template The path of the template to include.
 *
 * @return string the path to the visit template if viewing the visit endpoint.
 */
function filter_visit_template( $template ) {
	$rewrite_defs        = get_rewrite_defs();
	$visit_endpoint_flag = get_query_var( $rewrite_defs['visit']['tag'], '' );

	if ( '1' !== $visit_endpoint_flag ) {
		return $template;
	}

	enqueue_visit_scripts();

	add_filter( 'document_title_parts', __NAMESPACE__ . '\filter_visit_title' );
	add_filter( 'body_class', __NAMESPACE__ . '\filter_visit_body_class' );

	return CHECK_IN_PATH . 'templates/pub/visit.php';
}

add_filter( 'template_include', __NAMESPACE__ . '\filter_visit_template' );

/**
 * Enqueues scripts and styles for the visit endpoint.
 *
 * @since 1.0.0
 */
function enqueue_visit_scripts() {
	$min = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	wp_enqueue_style(
		'check_in',
		CHECK_IN_URL . "assets/css/check-in{$min}.css",
		array(),
		CHECK_IN_VERSION
	);

	wp_register_script(
		'check_in_react',
		"https://unpkg.com/react@15/dist/react{$min}.js",
		array(),
		'15',
		true
	);

	wp_register_script(
		'check_in_react_dom',
		"https://unpkg.com/react-dom@15/dist/react-dom{$min}.js",
		array( 'check_in_react' ),
		'15',
		true
	);

	wp_enqueue_script(
		'check_in',
		CHECK_IN_URL . "assets/js/check-in{$min}.js",
		array(
			'check_in_react',
			'check_in_react_dom',
			'jquery',
		),
		CHECK_IN_VERSION,
		true
	);

	$localize_data = array(
		'coworkers'       => \Check_In\Coworkers\get_coworkers(),
		'nonce'           => wp_create_nonce( get_submission_nonce_action() ),
		'nonce_name'      => get_submission_nonce_name(),
		'submission_flag' => get_submission_flag_name(),
		'submission_url'  => get_submission_url(),
	);

	wp_localize_script( 'check_in', 'check_in_data', $localize_data );
}

/**
 * Updates the title tag document parts for the visit endpoint.
 *
 * @since 1.0.0
 *
 * @param array $title The document title parts.
 *
 * @return array The document title parts for the visit endpoint.
 */
function filter_visit_title( $title ) {
	$title = array(
		'title' => esc_html__( 'Visitor Check-in', 'check_in' ),
		'site'  => get_bloginfo( 'name', 'display' )
	);

	return $title;
}

/**
 * Make sure the visit endpoint only has the proper body classes.
 *
 * @since 1.0.0
 *
 * @param array $classes An array of body classes.
 *
 * @return array An array of body classes for the visit endpoint.
 */
function filter_visit_body_class( $classes ) {
	$remove_classes = array(
		'home',
		'blog',
		'hfeed',
		'has-sidebar',
	);

	$classes = array_diff( $classes, $remove_classes );

	$classes[] = 'check_in_page';

	return $classes;
}
