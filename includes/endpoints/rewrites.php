<?php
/**
 * Rewrite Rule functions.
 *
 * This contains functions that flush rewrite rules and registers rewrite rules.
 *
 * @package Check_In\Endpoints
 * @since 1.0.0
 */

namespace Check_In\Endpoints;

/**
 * Registers rewrite rules for the definitions in get_rewrite_defs().
 *
 * @since 1.0.0
 * @see get_rewrite_defs()
 */
function register_rewrite_rules() {
	$rewrite_defs = get_rewrite_defs();

	foreach ( $rewrite_defs as $slug => $rewrite_def ) {
		$tag      = $rewrite_def['tag'];
		$priority = $rewrite_def['priority'];

		$rule_regex    = '^' . $slug . '\/{0,1}$';
		$rule_redirect = 'index.php?' . $tag . '=1';

		$tag_regex = '1';

		add_rewrite_rule( $rule_regex, $rule_redirect, $priority );
		add_rewrite_tag( '%' . $tag . '%', $tag_regex );
	}
}

add_action( 'check_in_init', __NAMESPACE__ . '\register_rewrite_rules' );

/**
 * Sets a flag used to flush rewrite rules on the init action.
 *
 * @since 1.0.0
 */
function set_flush_rewrites_flag() {
	update_option( get_flush_rewrites_flag_name(), 1 );
}

/**
 * Flushes rewrite rules if the flush rewrites flag is set.
 *
 * @since 1.0.0
 */
function flush_rewrites_maybe() {
	$flush_maybe = get_option( get_flush_rewrites_flag_name(), 0 );

	if ( 1 !== intval( $flush_maybe ) ) {
		return;
	}

	flush_rewrite_rules();

	delete_option( get_flush_rewrites_flag_name() );
}

add_action( 'check_in_init', __NAMESPACE__ . '\flush_rewrites_maybe', 100 );
