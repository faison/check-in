<?php
/**
 * The submit endpoint functions.
 *
 * @package Check_In\Endpoints
 * @since 1.0.0
 */

namespace Check_In\Endpoints;

/**
 * Returns the URL for the submit endpoint.
 *
 * @since 1.0.0
 *
 * @return string The URL for the submit endpoint.
 */
function get_submission_url() {
	return get_home_url() . '/' . get_submission_slug() . '/';
}

/**
 * Detects and handles Check-in submissions, returning success or failure depending on if a log is created.
 *
 * @see \Check_In\Visitor_Log\add_log()
 * @see wp_send_json_success()
 * @see wp_send_json_error()
 *
 * @since 1.0.0
 */
function handle_submission() {
	$rewrite_defs         = get_rewrite_defs();
	$submit_endpoint_flag = get_query_var( $rewrite_defs['visit/submit']['tag'], '' );

	if ( '1' !== $submit_endpoint_flag ) {
		return;
	}

	$submission_flag = filter_input( INPUT_POST, get_submission_flag_name(), FILTER_CALLBACK, array(
		'options' => 'sanitize_text_field',
	) );

	if ( '1' !== $submission_flag ) {
		wp_safe_redirect( get_visit_url() );
		exit;
	}

	if ( ! isset( $_POST[ get_submission_nonce_name() ] ) ||
	     ! wp_verify_nonce( $_POST[ get_submission_nonce_name() ], get_submission_nonce_action() ) ) {
		wp_send_json_error( array(
			'error' => 'invalid_nonce',
		) );
	}

	$meta_defs = \Check_In\Visitor_Log\get_meta_defs();
	$meta_args = array();

	// Gather and sanitize all the required meta values
	foreach ( $meta_defs as $meta_key => $meta_def ) {
		$value = filter_input( INPUT_POST, $meta_key, FILTER_CALLBACK, array(
			'options' => $meta_def['sanitize_callback'],
		) );

		if ( empty( $value ) ) {
			$value = '';
		}

		$meta_args[ $meta_key ] = $value;
	}

	$log_id = \Check_In\Visitor_Log\add_log( $meta_args );

	if ( ! is_wp_error( $log_id ) ) {
		wp_send_json_success( $log_id );
	}

	wp_send_json_error( array(
		'error' => $log_id->get_error_code(),
		'data'  => $log_id->get_error_data(),
	) );
}

add_action( 'wp', __NAMESPACE__ . '\handle_submission' );
