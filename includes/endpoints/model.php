<?php
/**
 * Rewrite Rule Model functions.
 *
 * This contains functions that return things like the rewrite rule regex, redirects, tags, etc..
 *
 * @package Check_In\Endpoints
 * @since 1.0.0
 */

namespace Check_In\Endpoints;

/**
 * Returns the Flush Rewrites Flag name.
 *
 * @since 1.0.0
 *
 * @return string The Flush Rewrites Flag name.
 */
function get_flush_rewrites_flag_name() {
	return 'check_in_flush_rewrites';
}

/**
 * Returns the visit endpoint slug.
 *
 * @since 1.0.0
 *
 * @return string The visit endpoint slug.
 */
function get_visit_slug() {
	return 'visit';
}

/**
 * Returns the submission endpoint slug.
 *
 * @since 1.0.0
 *
 * @return string The submission endpoint slug.
 */
function get_submission_slug() {
	return 'visit/submit';
}

/**
 * Returns the Rewrite Rule definitions.
 *
 * @since 1.0.0
 *
 * @return array The Rewrite Rule definitions.
 */
function get_rewrite_defs() {
	return array(
		get_visit_slug()      => array(
			'tag'       => 'check_in_visit',
			'priority'  => 'top',
		),
		get_submission_slug() => array(
			'tag'       => 'check_in_submission',
			'priority'  => 'top',
		),
	);
}

/**
 * Returns the name of the submission nonce.
 *
 * @since 1.0.0
 *
 * @return string The name of the submission nonce.
 */
function get_submission_nonce_name() {
	return 'check_in_nonce';
}

/**
 * Returns the submission nonce action.
 *
 * @since 1.0.0
 *
 * @return string The submission nonce action.
 */
function get_submission_nonce_action() {
	return 'check_in_submission';
}

/**
 * Returns the submission flag.
 *
 * @since 1.0.0
 *
 * @return string The submission flag.
 */
function get_submission_flag_name() {
	return 'check_in_submission_flag';
}
