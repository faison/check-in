<?php
/**
 * Plugin setup code.
 *
 * @since 1.0.0
 */

namespace Check_In;

/**
 * The plugin setup function.
 *
 * @since 1.0.0
 */
function setup() {
	add_action( 'init', __NAMESPACE__ . '\check_in_init' );

	do_action( 'check_in_loaded' );
}

/**
 * Initializes the plugin and fires an action for other parts of the plugin to hook into.
 *
 * @since 1.0.0
 */
function check_in_init() {
	do_action( 'check_in_init' );
}

/**
 * Runs plugin activation code.
 *
 * @since 1.0.0
 */
function activate() {
	\Check_In\Endpoints\set_flush_rewrites_flag();
	\Check_In\Coworkers\update_stored_coworkers();
}
